Libadwaita, Gtk4 Desktop Icons NG with GSconnect Integration, Drag and Drop onto Dock (Gtk4-DING)

Libadwaita/Gtk4 Port of Desktop Icons NG with updated and modified code base, uses gio menus, all functions are async where possible, multiple fixes and new features-

* Now uses libadwaita.
* Make Links on Desktop with Alt button on Wayland. Does not work with X11.
* Copied/dropped/pasted files retain dropped position.
* Right Click Menus will not go under the dock.
* Better multi monitor support, preference to place icons on non primary monitor.
* GSconnect extension integration, can send files from desktop directly to mobile device.
* Drag and drop Favorite apps from Dash to Dock, Dash to Panel directly to Desktop or remove from favorites.
* Improved gesture switching of workspaces, icons appear to be on all workspaces in moving windows.
* Support for dragging icons onto the dock - Drag icons from desktop to and drop over application icon to open them with the app. Works with Dash to Dock and Dash to Panel.
* Support for dragging icons from desktop directly to Trash on Dash to Dock or to mounted volumes to copy them directly.
* Display GIMP thumbnails, even for snap and flatpack installs.

Please see Readme for full details of new features. Works best on Wayland. There is a bug in GJS on X11, please see Readme. This extension now works with some hacks, even on X11. However your mileage may vary and use with caution on X11 and report any issues.

Please report all issues on the gitlab link below, this page is not monitored. All known issues are detailed there.

Extension Homepage https://gitlab.com/smedius/desktop-icons-ng
